/*jslint node:true */
'use strict';

var express = require('express'),
    mongoose = require('mongoose'),
    path = require('path'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    express_validator = require('express-validator'),
    config = require('./config/database'),
    passport = require('passport');

// start here
var app = express();

// mongo and mongoose
mongoose.connect(config.database);
var db = mongoose.connection;

// successful connection
db.once('open', function () {
  console.log('Connected to mongodb > mongodb://192.168.100.10:27017/nodekb');
});

// DB errors
db.on('error', function (err) {
  console.log(err);
});

// bring in models
var Article = require('./models/article');

// load view-engine
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'pug');

//  Body Parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//  set public folder
app.use(express.static(path.join(__dirname, 'public')));

//  express session middleware
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));

//  messages middleware
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//  validator middleware
app.use(express_validator({
  errorFormatter: function(param, msg, value) {
    var namespace = param.split('.'),
    root = namespace.shift(),
    formParam = root;
    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

//  passport config
require('./config/passport')(passport);

//  passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.get('*', function(req, res, next){
  res.locals.user = req.user || null;
  next();
});

/***************** routes-start *****************/

//  index page
app.get('/', function (req, res) {
  Article.find({}, function(err, articles){
    if(err){
      console.log(err);
    } else {
        res.render('index', {
        title:"Hello",
      });
    }
  });
});

// route form-inline articles
var articles = require('./routes/articles');
app.use('/articles', articles);

// route form-inline users
var users = require('./routes/users');
app.use('/users', users);


/***************** routes-end *****************/

// start server
app.listen(3000, function () {
  console.log('Server started on port 3000');
});
