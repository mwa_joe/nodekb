var express = require('express');

var router = express.Router();

// bring in Article model
var Article = require('../models/article');
var User = require('../models/user');


//  articles page
router.get('/', function (req, res) {
  Article.find({}, function(err, articles){
    if(err){
      console.log(err);
    } else {
        res.render('articles', {
        title:"Articles",
        articles: articles
      });
    }
  });
});

//  get an article
router.get('/add', ensureAuthenticated, function (req, res) {
  res.render('add_article', {
    title: "Add Article"
  });
});

//  post a new article
router.post('/add', function(req, res){
  req.checkBody('title', 'Title is required').notEmpty();
  req.checkBody('body', 'The body is required').notEmpty();

  // get error if any
  var errors = req.validationErrors();

  if (errors){
    res.render('add_article', {
      title: "Add Article",
      errors: errors,
      author: req.user._id
    });
  } else {
    var article = new Article();
    article.title = req.body.title;
    article.author = req.user._id;
    article.body = req.body.body;
    article.save(function(err){
      if(err){
        console.log(err);
        return;
      } else{
        req.flash('success', "'"+article.title+"' added");
        res.redirect('/articles');
      }
    });
  }

});

//  edit an article by its id
router.get('/edit/:id', ensureAuthenticated, function (req, res) {
  Article.findById(req.params.id, function(err, article){
    if(article.author != req.user._id){
      req.flash('danger', 'Not authorized');
      res.redirect('/');
    }
    res.render('edit_article', {
      title:"Edit Article",
      article:article
    });
  });
});

//  update submit post route
router.post('/edit/:id', function(req, res){
  var article = {};
  article.title = req.body.title;
  article.author = req.body.author;
  article.body = req.body.body;
  var query = {_id:req.params.id};
  Article.update(query, article, function(err){
    if(err){
      console.log(err);
      return;
    } else{
      req.flash('success', "'"+article.title+"' updated");
      res.redirect('/articles');
    }
  });
});

//  deleting an article
router.delete('/:id', ensureAuthenticated, function(req, res){
  if(!req.user._id){
    res.status(500).send();
  }
  var query = {_id:req.params.id};

  Article.findById(req.params._id, function(err, article){
    if (article.author != req.user._id){
      res.status(500).send();
    } else {
      Article.remove(query, function(err){
        if (err){
          console.log(err);
        }
        res.send('success');
      });
    }
  });
});

//  load an article though its id
router.get('/:id', function(req, res){
  Article.findById(req.params.id, function(err, article){
    User.findById(article.author, function(err, user){
      res.render('article', {
        article: article,
        author: user.name
        });
      });
    });
});

// access control
function ensureAuthenticated(req, res, next){
  if (req.isAuthenticated()){
    return next();
  }else {
    req.flash('danger', 'You are not logged in');
    res.redirect('/users/login');
  }
}

module.exports = router;
