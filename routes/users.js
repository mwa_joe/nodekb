var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var passport = require('passport');

// user model
var User = require('../models/user');

// register form
router.get('/register', function(req, res){
  res.render('register');
});

// register process
router.post('/register', function(req, res){
  var name = req.body.name;
  var username = req.body.username;
  var email = req.body.email;
  var password = req.body.password;
  var password2 = req.body.password2;

  req.checkBody('name', 'A name is required').notEmpty();
  req.checkBody('email', 'An Email is required').notEmpty();
  req.checkBody('email', 'The Email is not valid').isEmail();
  req.checkBody('username', 'A username is required').notEmpty();
  req.checkBody('password', 'A password is required').notEmpty();
  req.checkBody('password2', 'Passwords do not patch').equals(req.body.password);

  var errors = req.validationErrors();

  if (errors)  {
    res.render('register', {
      errors : errors
    });
  } else
  {
    var newUser = new User({
      name : name,
      email : email,
      username: username,
      password: password
    });
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(newUser.password, salt, function(err, hash){
        if (err){
          console.log(err);
        } else {
          newUser.password = hash;
          newUser.save(function(err){
            if (err){
              console.log(err);
              return;
            } else {
              req.flash('success', 'You are now registered and can log in');
              res.redirect('/users/login');
            }
          });
        }
      });
    });
  }
});

//  login form
router.get('/login', function(req, res){
  res.render('login');
});

//  login process
router.post('/login', function(req, res, next){
  passport.authenticate('local', {
    successRedirect:'/articles',
    failureRedirect:'/users/login',
    failureFlash: true
  })(req, res, next);
});

//  logout
router.get('/logout', function(req, res){
  req.logout();
  req.flash('success', "You have been logged out");
  res.redirect('/users/login');
});

module.exports = router;
